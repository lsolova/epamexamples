'use strict';

function LocalStorageObject() {
    this.store = window.localStorage;
}

LocalStorageObject.prototype.get = function (key) {
    return this.store.getItem(key);
};
LocalStorageObject.prototype.set = function (key, value) {
    this.store.setItem(key, value);
};
LocalStorageObject.prototype.remove = function (key) {
    this.store.removeItem(key);
};