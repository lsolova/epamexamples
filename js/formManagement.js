'use strict';

function FormManagement() {
    var sSubmitE = document.getElementById('ssubmit'),
        sKeyE = document.getElementById('skey'),
        sValueE = document.getElementById('svalue'),
        
        localStorageInstance = new LocalStorageObject(),
        indexedDbInstance = new IndexedDbObject(),
        
        fm = this;
    
    sSubmitE.addEventListener('click', function doOnSubmit() {
        var svalue = sValueE.value,
            skey = sKeyE.value,
            selectedEngine = document.querySelector('#sengine option:checked'),
            usedEngine = selectedEngine.value == 'ls' ? localStorageInstance : indexedDbInstance,
            getResult;
        if (svalue.length <= 0) {
            getResult = usedEngine.get(skey);
            if (typeof getResult === 'string') {
                fm.log(skey + ':  ' + getResult);
            } else {
                fm.log(skey + ':  Query result');
                getResult.then(function (resultValue) {
                    fm.log(skey + ':  ' + JSON.stringify(resultValue));
                });
            }
        } else {
            usedEngine.set(skey, svalue);
        }
    });
    
    return false;
}

FormManagement.prototype.log = function (message) {
    var resultStoreE = document.getElementById('resultStore'),
        newItem = document.createElement('li');
    newItem.innerHTML = message;
    resultStoreE.appendChild(newItem);
}