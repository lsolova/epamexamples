'use strict';

function IndexedDbObject() {
    this.db = window.indexedDB,
    this.dbOpenPromise;
}

IndexedDbObject.prototype.openDb = function () {
    var idbo = this;
    this.dbOpenPromise = this.dbOpenPromise || new Promise(function dobOpenPromiseCallback(resolve, reject) {
        var request = idbo.db.open('exampleDb', 1);
        
        request.onupgradeneeded = function doDbUpgrade() {
            var odb = request.result;
            odb.createObjectStore('sampleObjectStore', { keyPath: 'sampleId', autoIncrement: true });
        }
        request.onsuccess = function () {
            resolve(request.result);
        }
        request.onerror = function () {
            reject(request.error);
        }
    });
    return this.dbOpenPromise;
}

IndexedDbObject.prototype.openTransaction = function (openedDB, writable) {
    var trxPromise = new Promise(function promiseCallbackOfOpenTrx(resolve, reject) {
        var trx;
        if (writable) {
            trx = openedDB.transaction('sampleObjectStore', 'readwrite');
        } else {
            trx = openedDB.transaction('sampleObjectStore');
        }
        resolve(trx);
    });
    return trxPromise;
}

IndexedDbObject.prototype.get = function (key) {
    var idbo = this;
    return idbo.openDb()
        .then(function (openedDb) {
            return idbo.openTransaction(openedDb, false); 
        })
        .then(function (trx) {
            var resultPromise = new Promise(function getResultPromise(resolve, reject) {
                var objStore = trx.objectStore('sampleObjectStore'),
                    request = objStore.get(key);
                request.onsuccess = function () {
                    resolve(request.result);
                }
                request.onerror = function () {
                    reject(request.error);
                }
            });
            return resultPromise;
        });
}

IndexedDbObject.prototype.set = function (key, value) {
    var idbo = this;
    return idbo.openDb()
        .then(function (openedDb) {
            return idbo.openTransaction(openedDb, true); 
        })
        .then(function (trx) {
            var promise = new Promise(function (resolve, reject) {
                var objStore = trx.objectStore('sampleObjectStore'),
                    request;
                request = objStore.put({sampleId: key, sampleContent: value});
                request.onsuccess = function () {
                    resolve(request.result);
                };
            });
            return promise;
        });
}
